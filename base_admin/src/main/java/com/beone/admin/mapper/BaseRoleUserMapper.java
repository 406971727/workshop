package com.beone.admin.mapper;


import com.base.SuperMapper;
import com.beone.admin.entity.BaseRoleUser;

/**
 * 运维数据_用户对应的角色 Mapper 接口
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
public interface BaseRoleUserMapper extends SuperMapper<BaseRoleUser> {

}
