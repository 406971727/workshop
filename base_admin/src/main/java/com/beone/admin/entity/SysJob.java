package com.beone.admin.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.base.common.BaseModel;

/**
 * @Title  实体类
 * @Author 覃球球
 * @Version 1.0 on 2018-10-25
 * @Copyright 贝旺科权
 */
@TableName("base_sys_job")
public class SysJob extends BaseModel {

    private static final long serialVersionUID = 1L;

	private String id;
    /**
     * 描述任务
     */
	@TableField("job_name")
	private String jobName;
    /**
     * 任务表达式
     */
	private String cron;
    /**
     * 状态:0未启动false/1启动true
     */
	private Integer status;
    /**
     * 任务执行方法
     */
	@TableField("clazz_path")
	private String clazzPath;
    /**
     * 其他描述
     */
	@TableField("job_desc")
	private String jobDesc;
	@TableField("create_by")
	private String createBy;
	@TableField("create_date")
	private Date createDate;
	@TableField("update_by")
	private String updateBy;
	@TableField("update_date")
	private Date updateDate;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getClazzPath() {
		return clazzPath;
	}

	public void setClazzPath(String clazzPath) {
		this.clazzPath = clazzPath;
	}

	public String getJobDesc() {
		return jobDesc;
	}

	public void setJobDesc(String jobDesc) {
		this.jobDesc = jobDesc;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "SysJob{" +
			", id=" + id +
			", jobName=" + jobName +
			", cron=" + cron +
			", status=" + status +
			", clazzPath=" + clazzPath +
			", jobDesc=" + jobDesc +
			", createBy=" + createBy +
			", createDate=" + createDate +
			", updateBy=" + updateBy +
			", updateDate=" + updateDate +
			"}";
	}
}
