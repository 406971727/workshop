package com.beone.admin.utils.result;

/**
 * @Title
 * @Author 庞绪 on 2018/3/30.
 * @Copyright ©长沙科权
 */
public class Result<T> {

    //返回标记  0:错误 1:成功
    private int flag;
    //错误编号
    private String code;
    //返回信息
    private String msg;
    //数据
    private T data;
    //token
    private String token;

    Result(int flag, String code, T data, String token) {
        this.flag = flag;
        this.code = code;
        this.data = data;
        this.token = token;
    }


    Result(int flag, ResultCode resultCode) {
        this(flag, resultCode.getKey(), null, null);
        this.msg = resultCode.getValue();
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
