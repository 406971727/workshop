package com.beone.admin.controller.base;


import com.alibaba.fastjson.JSON;
import com.base.SuperController;
import com.beone.admin.annotation.Log;
import com.beone.admin.common.AdminConstants;
import com.beone.admin.entity.BasePermissionRole;
import com.beone.admin.entity.BaseRole;
import com.beone.admin.exception.MvcException;
import com.beone.admin.service.BasePermissionRoleService;
import com.beone.admin.service.BaseRoleService;
import com.beone.admin.utils.result.ResultCode;
import com.beone.admin.utils.result.ResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Title 运维数据_角色信息 前端控制器
 * @Author 覃球球
 * @Version 1.0 on 2018-01-25
 * @Copyright 长笛龙吟
 */
@Controller
@RequestMapping("/system/role")
@Api(value = "角色管理模块", tags = {"角色管理接口"})
public class BaseRoleController extends SuperController {

    private static final Logger log = LoggerFactory.getLogger(BaseRoleController.class);

    @Autowired
    private BaseRoleService roleService;

    @Autowired
    BasePermissionRoleService permissionRoleService;

    /**
     * 角色管理 入口
     * @return
     */
    @RequestMapping("")
    public String index(){
        return "base/role/index";
    }

    /**
     * 角色管理 显示添加页面
     * @return
     */
    @RequestMapping("/showAdd")
    public String showAdd(){
        return "base/role/form";
    }

    /**
     * 角色管理 显示修改页面
     * @return
     */
    @RequestMapping("/showUpdate/{roleId}")
    public String showUpdate(@PathVariable("roleId") String roleId, ModelMap model)throws MvcException{
        try{
            if(StringUtils.isNotBlank(roleId) && roleId.matches("[\\d+]")){
                Integer id = Integer.valueOf(roleId);
                model.addAttribute("roleId", roleId);
                model.addAttribute("role", roleService.selectById(id));
                List<BasePermissionRole> list = permissionRoleService.getPermissionIdByRoleId(id);
                model.addAttribute("rolePermission", JSON.toJSONString(list));
            }
        }catch (Exception e){
            log.error("showUpdate 异常 e = {}", e);
            throw new MvcException(e.getMessage());
        }
        return "base/role/form";
    }

    /**
     * 获取所有可用的角色信息
     * @return
     */
    @ApiOperation(value = "获取所有可用的角色信息", notes = "获取所有可用的角色信息")
    @PostMapping("allRoles")
    @ResponseBody
    public Object getAllRoles(){
        return roleService.getAllUsedRoles();
    }

    /**
     * 角色分页列表 JSON
     * @param role
     * @param currPage
     * @param pageSize
     * @return
     */
    @ApiOperation(value = "角色列表", notes = "角色列表分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", required = true
                    , paramType = "param", dataType = "int"),
            @ApiImplicitParam(name = "rows", value = "每页最大显示记录数", required = true
                    , paramType = "param", dataType = "int")
    })
    @RequestMapping(value = "/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE
            , method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Object getUserList(BaseRole role,
                              @RequestParam(value = "page", defaultValue = AdminConstants.DEFAULT_PAGE_NUM) int currPage,
                              @RequestParam(value = "rows", defaultValue = AdminConstants.PAGE_SIZE) int pageSize) {
        return  roleService.getRolePagination(role,currPage,pageSize);
    }

    /**
     * 启用/禁用角色
     * @param roleId    用户id
     * @param isUse  更新当前状态  1 启用 0 禁用
     * @return 操作结果
     */
    @PostMapping(value = "/status", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object status(Integer roleId, Integer isUse) {
        BaseRole role = new BaseRole();
        role.setRoleId(roleId);
        role.setIsUse(isUse);
        if (roleService.updateById(role)) {
            return ResultUtils.success();
        }
        return ResultUtils.warn(ResultCode.FAIL);
    }

    /**
     * 删除角色
     *
     * @param roleId 角色Id
     * @return 操作结果
     */
    @Log(desc = "删除角色", type = Log.LOG_TYPE.DEL)
    @PostMapping(value = "/delRole", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @ApiOperation(value = "删除角色", notes = "根据角色Id，删除角色")
    @ApiImplicitParam(name = "roleId", value = "角色ID", required = true, paramType = "param", dataType = "int")
    public Object delRole(Integer roleId) {
        if (roleService.delRole(roleId)) { //根据Id删除角色信息
            return super.responseResult("success", "成功！");
        }
        return  super.responseResult("fail", "失败！");
    }


    /**
     * 保持角色信息
     *
     * @param role 角色信息
     * @return 保存结果
     */
    @Log(desc = "角色", type = Log.LOG_TYPE.SAVE)
    @ApiOperation(value = "保存角色", notes = "保存角色信息 主键属性为空新增,否则修改")
    @ApiImplicitParam(name = "role", value = "角色详细实体role", required = true
            , paramType = "form", dataType = "object")
    @PostMapping(value = "/saveRole", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object saveUser(BaseRole role) {
        if (roleService.saveRole(role)) {
            return super.responseResult("success", "成功！");
        }
        return super.responseResult("fail", "失败！");
    }

   /* *//**
     * 根据角色Id，显示角色对应权限信息
     *
     * @param roleId 角色信息
     *//*
    @RequestMapping(value = "/showRolePermissions/{roleId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object showRolePermissions(@PathVariable("roleId") Integer roleId) {
        return roleService.showRolePermissionses(roleId);
    }

    *//**
     * 根据角色Id，显示角色对应权限信息
     *
     * @param roleId 角色信息
     *//*
    @RequestMapping(value = "/ownerPermission/{roleId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public Object ownerPermission(@PathVariable("roleId") Integer roleId) {
        return roleService.setOwnRolePermissions(roleId);
    }*/
}

