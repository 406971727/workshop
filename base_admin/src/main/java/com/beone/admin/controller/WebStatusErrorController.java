package com.beone.admin.controller;

import com.beone.admin.exception.MvcException;
import org.springframework.boot.autoconfigure.web.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.BasicErrorController;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @title  web status error 处理
 * @Author 覃球球
 * @Version 1.0 on 2018/1/31.
 * @Copyright 长笛龙吟
 */
@Controller
@RequestMapping(value = "error")
public class WebStatusErrorController  implements ErrorController {
    /**
     * 403 错误
     * @return
     */
    @RequestMapping("/403")
    public String notPermision(){
        return "common/error/403";
    }

    /**
     * 400 错误
     * @return
     */
    @RequestMapping("/400")
    public String badRequest(){
        return "common/error/400";
    }

    /**
     * 401 错误
     * @return
     */
    @RequestMapping("/401")
    public String unAuthorRequest(){
        return "common/error/401";
    }

    /**
     * 404 错误
     * @return
     */
    @RequestMapping("/404")
    public String notFound(){
        return "common/error/404";
    }

    /**
     * 500 错误
     * @return
     */
    @RequestMapping("/500")
    public String serverError(){
        return "common/error/500";
    }

    /**
     * Spring Boot error 自定义
     * @return
     * @throws MvcException
     */
    @RequestMapping
    public String  showError() throws MvcException {
        return getErrorPath();
    }

    /**
     * Returns the path of the error page.
     *
     * @return the error path
     */
    @Override
    public String getErrorPath() {
        return "common/error/500";
    }
}