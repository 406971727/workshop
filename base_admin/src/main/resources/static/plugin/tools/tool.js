(function () {
    var layui = {
        /*
         参数解释：
         title   标题
         url     请求的url
         id      需要操作的数据id
         w       弹出层宽度（缺省调默认值）
         h       弹出层高度（缺省调默认值）
         */
        dialog : function (id,title, url, w, h) {
            if (title == null || title == '') {
                title = false;
            }

            if (url == null || url == '') {
                url = "/error/404";
            }

            if (w == null || w == '') {
                w = ($(window).width() * 0.9);
            }

            if (h == null || h == '') {
                h = ($(window).height() - 50);
            }

            layer.open({
                id: id,
                type: 2,
                area: [w + 'px', h + 'px'],
                fix: false,
                maxmin: true,
                shadeClose: false,
                shade: 0.4,
                title: title,
                content: url
            });
        },
        doRequest : function (url, data, callback) {
            $.ajax({
                url:url,
                type:"post",
                data:data,async:false,
                success:function(d){
                    if(d.code == 'success'){
                        window.top.layer.msg(d.desc,{icon:6,offset: 'rb',area:['120px','80px'],anim:2});
                        if(callback && typeof callback == 'function'){
                            callback();
                        }
                    }else{
                        window.top.layer.msg(d.desc,{icon:5,offset: 'rb',area:['120px','80px'],anim:2});
                    }
                },error:function(){
                    alert('error');
                }
            });
        },
        confirm : function (msg, callback) {
            return layer.confirm(msg, { btn: ['确认', '取消']},  callback)
        },
        toastMsg : function (opt) {
            var defaults = {content:'', icon:'info', anim:'default',time:3000};
            opt = $.extend(defaults, opt);
            var icon = 0, anim = 0;
            switch (opt.icon){
                case 'fail':
                    icon = 5;
                    break;
                case 'success':
                    icon = 6;
                    break;
            }
            switch (opt.anim){
                case 'fail':
                    anim = 6;
                    break;
                default:
                    anim = 0;
            }
            layer.open({content: opt.content, closeBtn:0,title:'',btn:[],icon: icon,time:opt.time,anim: anim});
        }, onEnter : function(callback){
            document.onkeydown = function (e) { // 回车提交表单
                var theEvent = window.event || e;
                var code = theEvent.keyCode || theEvent.which;
                if (code == 13) {
                    if(callback && typeof callback == 'function'){
                        callback();
                    }
                }
            }
        }
    };
    window.layuiUtils = layui;
})()


//通用
function popup(title, url, w, h, id) {
    if (title == null || title == '') {
        title = false;
    }
    ;
    if (url == null || url == '') {
        url = "error/404";
    }
    ;
    if (w == null || w == '') {
        w = ($(window).width() * 0.9);
    }
    ;
    if (h == null || h == '') {
        h = ($(window).height() - 50);
    }
    ;
    layer.open({
        id: id,
        type: 2,
        area: [w + 'px', h + 'px'],
        fix: false,
        maxmin: true,
        shadeClose: false,
        shade: 0.4,
        title: title,
        content: url
    });
}

/**
 * 父窗口弹出
 * @param url
 * @param data
 * @param tableId
 */
function postAjaxre(url, data, tableId) {
    $.ajax({
        url: url,
        type: "post",
        data: data,
        dataType: "json", traditional: true,
        success: function (data) {
            if (data.flag) {
                var index = parent.layer.getFrameIndex(window.name);
                parent.layer.close(index);
                window.parent.layui.table.reload(tableId);
                window.top.layer.msg(data.msg, {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
            } else {
                layer.msg(data.msg, {icon: 5, offset: 'rb', area: ['120px', '80px'], anim: 2});
            }
        }
    });
}

function layerAjax(url, data, tableId) {
    $.ajax({
        url: url,
        type: 'post',
        data: data,
        traditional: true,
        success: function (d) {
            var index = parent.layer.getFrameIndex(window.name);
            if (d.code == "success") {
                parent.layer.close(index);
                window.parent.layui.table.reload(tableId);
                window.top.layer.msg(d.desc, {icon: 6, offset: 'rb', area: ['200px', '80px'], anim: 2});
            } else {
                layer.msg(d.desc, {icon: 5});
            }
        }, error: function (e) {
            layer.alert("发生错误", {icon: 6}, function () {
                var index = parent.layer.getFrameIndex(window.name);
                parent.layer.close(index);
            });
        }
    });
}

function eleClick(active, ele) {
    $(ele).on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
}