package com.base;

import java.util.HashMap;
import java.util.Map;

/**
 * @title  控制器父类
 * @Author 覃球球
 * @Version 1.0 on 2018/8/30.
 * @Copyright 长笛龙吟
 */
public abstract class SuperController {
    /**
     *  响应客户端消息
     * @param flag
     * @param msg
     * @return
     */
     protected Map<String, Object> responseResult(boolean flag, String msg){
         Map<String, Object>  json = new HashMap<String, Object>();
         json.put("flag", flag);
         json.put("msg", msg);
         return json;
     }

    /**
     *  响应客户端消息
     * @param code
     * @param msg
     * @return
     */
    protected Map<String, Object> responseResult(String code, String msg){
        Map<String, Object>  json = new HashMap<String, Object>();
        json.put("code", code);
        json.put("desc", msg);
        return json;
    }

    /**
     *  响应客户端消息
     * @param code
     * @param msg
     * @return
     */
    protected Map<String, Object> responseResult(String code, String msg, Object data){
        Map<String, Object>  json = new HashMap<String, Object>();
        json.put("code", code);
        json.put("msg", msg);
        json.put("data", data);
        return json;
    }
}
