package com.base;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

/**
 * @title  业务层接口抽象实现类
 *              过滤 MySQL like 查询的特殊字符
 *                  参考 BaseStringUtils.filterSpecSpecialValue(value)
 * @Author 覃球球
 * @Version 1.0 on 2017/12/18.
 * @Copyright 长笛龙吟
 */
public class SuperServiceImpl<M extends SuperMapper<T>, T> extends ServiceImpl<M,T> {


}
