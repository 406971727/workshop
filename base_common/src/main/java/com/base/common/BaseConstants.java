package com.base.common;

/**
 * @title  常量
 * @Author 覃球球
 * @Version 1.0 on 2017/12/18.
 * @Copyright 长笛龙吟
 */
public interface BaseConstants {

    String YYYY_MM_DD = "yyyy-MM-dd";

    String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    String DEFAULT_CHARACTER = "UTF-8";

    String EMPTY_STRING="";
}
