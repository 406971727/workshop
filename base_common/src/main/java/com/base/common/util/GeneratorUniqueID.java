package com.base.common.util;

import java.util.UUID;

/**
 * @Title  生成唯一ID
 * @Author 覃忠君 on 2016/10/24.
 * @Copyright © 长笛龙吟
 */
public class GeneratorUniqueID {
    private static String[] chars = new String[] { "a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z" };

    /**
     * 基于UUID类生成唯一短码
     * @return
     */
    public static String createToken() {
        StringBuilder shortBuffer = new StringBuilder();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        int length = 8; //生成Token默认长度

        for (int i = 0; i < length; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x3E]);
        }
        return shortBuffer.toString();
    }

    private static String getUUID(){
        String uuid = UUID.randomUUID().toString();
        return uuid;
    }

    /**
     * 单据号
     * @return
     */
    public static String getOrderId(Integer len){ //单据号
        String uuid = getUUID();
        int hashCode = uuid.hashCode();
        hashCode = Math.abs(hashCode);
        return String.format("%0" + len + "d", hashCode);
    }

    /**
     * 单据号
     * @return
     */
    public static String getOrderId(){ //单据号
        return getOrderId(11);
    }
}
